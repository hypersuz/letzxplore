﻿create table UserTable
(
UserId Varchar(255) NOT NULL PRIMARY KEY,
Name varchar(255) NOT NULL,
EmailId varchar(255) NOT NULL,
Password varchar(255) NOT NULL
)
Insert into UserTable(UserId,Name,EmailId,Password) Values('u1','matt','matt@a.com','aaa');
Insert into UserTable(UserId,Name,EmailId,Password) Values('u2','sesha','sesha@b.com','bbb');
Insert into UserTable(UserId,Name,EmailId,Password) Values('u3','rex','rex@c.com','ccc');

create table QuestionTable
(
QuestionId varchar(255) NOT NULL PRIMARY KEY,
UserId Varchar(255) NOT NULL,
Title varchar(255) NOT NULL,
XMLLocation varchar(255) NOT NULL,
Likes INT
Foreign Key(UserId) References UserTable(UserId)
)

Insert into QuestionTable(QuestionId,UserId,Title,XMLLocation) values('q1','u1','How to correctly use Html.ActionLink with ASP.NET MVC 4 Areas','local');
Insert into QuestionTable(QuestionId,UserId,Title,XMLLocation) values('q2','u2','C# GetType() returning differently on different machines','local');


create table AnswerTable
(
AnswerId varchar(255) NOT NULL PRIMARY KEY,
QuestionId varchar(255) NOT NULL,
UserId varchar(255) NOT NULL,
XMLLocation varchar(255) NOT NULL,
Likes INT,
Foreign Key(QuestionId) References QuestionTable(QuestionId),
Foreign Key(UserId) References UserTable(UserId)
)

Insert into AnswerTable(AnswerId,QuestionId,UserId,XMLLocation) values ('a1','q1','u1','local');
Insert into AnswerTable(AnswerId,QuestionId,UserId,XMLLocation) values ('a1','q1','u2','local');
Insert into AnswerTable(AnswerId,QuestionId,UserId,XMLLocation) values ('a1','q2','u2','local');
Insert into AnswerTable(AnswerId,QuestionId,UserId,XMLLocation) values ('a1','q1','u3','local');


create table FileTable
(
QuestionId varchar(255),
AnswerId varchar(255),
PdfLink varchar(255),
WordLink varchar(255),
ImageLink varchar(255),
VideoLink varchar(255)
)
Insert into FileTable(QuestionId,PdfLink) values ('q1','aaaaaaaa');
Insert into FileTable(AnswerId,PdfLink) values('a1','bbbbbb');
