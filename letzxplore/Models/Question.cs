﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace letzxplore.Models
{
    public class Question
    {
        public string QID { get; set; }

        public string Qtitle { get; set; }

        public string UID { get; set; }

        public string Description { get; set; }

        public int Likes { get; set; }

        public List<Answer> Answers { get; set; }

        public List<Comment> Comments { get; set; }

        public List<File> Files { get; set; }

    }
}