using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Xml;

namespace letzxplore.Models
{
    public class HomeModels
    {
        private SqlConnection conn = new SqlConnection("Data Source=SYNCLAPN10107;Initial Catalog = letzexplore; Integrated Security = True; Connect Timeout = 15; Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;MultipleActiveResultSets=true");

        public List<Question> getTopQuestions()
        {
            List<Question> topQuestions = new List<Question>();
            this.conn.Open();
            SqlCommand cmd;
            string query = "SELECT TOP 10 * FROM QuestionTable ORDER BY Likes DESC";
            cmd = new SqlCommand(query, this.conn);
            cmd.ExecuteNonQuery();
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                if (rdr != null)
                {
                    while (rdr.Read())
                    {
                        topQuestions.Add(fillQuestionDeatailsById(rdr.GetString(rdr.GetOrdinal("QuestionId"))));
                    }
                }
            }

            return topQuestions;
        }

        public Question fillQuestionDeatailsById(string id)
        {
            Question ques = new Question();
            SqlCommand cmd1;
            string query = "SELECT * FROM QuestionTable WHERE QuestionId = '" + id +"'";
            cmd1 = new SqlCommand(query, this.conn);
            cmd1.ExecuteNonQuery();
            using (SqlDataReader rdr1 = cmd1.ExecuteReader())
            {
                if (rdr1 != null)
                {
                    while (rdr1.Read())
                    {
                        ques.QID = rdr1.GetString(rdr1.GetOrdinal("QuestionId"));
                        ques.Qtitle = rdr1.GetString(rdr1.GetOrdinal("Title"));
                        ques.UID = rdr1.GetString(rdr1.GetOrdinal("UserId"));
                        ques.Likes = rdr1.GetInt32(rdr1.GetOrdinal("Likes"));
                        ques.Description = ques.QID;
                        query = "SELECT * FROM AnswerTable WHERE QuestionId = '" + ques.QID + "'";
                        SqlCommand cmd11 = new SqlCommand(query, this.conn);
                        cmd11.ExecuteNonQuery();
                        using (SqlDataReader rdr11 = cmd11.ExecuteReader())
                        {
                            if (rdr11 != null)
                            {
                                while (rdr11.Read())
                                {
                                    ques.Answers.Add(fillAnswerDeatailsById(rdr11.GetString(rdr11.GetOrdinal("AnswerId"))));
                                }
                            }
                        }
                        this.conn.Close();
                        this.conn.Open();
                        query = "SELECT * FROM A WHERE QuestionId = '" + ques.QID + "'";
                        SqlCommand cmd12 = new SqlCommand(query, this.conn);
                        cmd12.ExecuteNonQuery();
                        using (SqlDataReader rdr12 = cmd12.ExecuteReader())
                        {
                            if (rdr12 != null)
                            {
                                while (rdr12.Read())
                                {
                                    ques.Comments.Add(fillCommentDeatailsById(rdr12.GetString(rdr12.GetOrdinal("CommentId"))));
                                }
                            }
                        }

                        query = "SELECT * FROM FileTable WHERE QuestionId = '" + ques.QID + "'";
                        SqlCommand cmd13 = new SqlCommand(query, this.conn);
                        cmd13.ExecuteNonQuery();
                        using (SqlDataReader rdr13 = cmd13.ExecuteReader())
                        {
                            if (rdr13 != null)
                            {
                                while (rdr13.Read())
                                {
                                    ques.Files.Add(fillFileDeatailsById(rdr13.GetString(rdr13.GetOrdinal("FileId"))));
                                }
                            }
                        }
                    }
                }
            }

            return ques;
        }

        public Answer fillAnswerDeatailsById(string id)
        {
            Answer ans = new Answer();
            SqlCommand cmd2;
            string query = "SELECT * FROM AnswerTable WHERE AnswerId = '" + id + "'";
            cmd2 = new SqlCommand(query, this.conn);
            cmd2.ExecuteNonQuery();
            using (SqlDataReader rdr2 = cmd2.ExecuteReader())
            {
                if (rdr2 != null)
                {
                    while (rdr2.Read())
                    {
                        ans.AID = rdr2.GetString(rdr2.GetOrdinal("AnswerId"));                        
                        ans.UID = rdr2.GetString(rdr2.GetOrdinal("UserId"));
                        ans.Likes = rdr2.GetInt32(rdr2.GetOrdinal("Likes"));
                        ans.Description = getXmlData(getXMLpath(ans.AID, "answer"));
                        query = "SELECT * FROM A WHERE AnswerId = '" + ans.AID + "'";
                        SqlCommand cmd21 = new SqlCommand(query, this.conn);
                        cmd21.ExecuteNonQuery();
                        using (SqlDataReader rdr21 = cmd21.ExecuteReader())
                        {
                            if (rdr21 != null)
                            {
                                while (rdr21.Read())
                                {
                                    ans.Comments.Add(fillCommentDeatailsById(rdr21.GetString(rdr21.GetOrdinal("CommentId"))));
                                }
                            }
                        }

                        query = "SELECT * FROM FileTable WHERE AnswerId = '" + ans.AID + "'";
                        SqlCommand cmd22 = new SqlCommand(query, this.conn);
                        cmd22.ExecuteNonQuery();
                        using (SqlDataReader rdr22 = cmd22.ExecuteReader())
                        {
                            if (rdr22 != null)
                            {
                                while (rdr22.Read())
                                {
                                    ans.Files.Add(fillFileDeatailsById(rdr22.GetString(rdr22.GetOrdinal("FileId"))));
                                }
                            }
                        }
                    }
                }
            }

            return ans;
        }

        public Comment fillCommentDeatailsById(string id)
        {
            Comment com = new Comment();
            SqlCommand cmd3;
            string query = "SELECT * FROM A WHERE CommentId = '" + id + "'";
            cmd3 = new SqlCommand(query, this.conn);
            cmd3.ExecuteNonQuery();
            using (SqlDataReader rdr3 = cmd3.ExecuteReader())
            {
                if (rdr3 != null)
                {
                    while (rdr3.Read())
                    {
                        if(rdr3.GetString(rdr3.GetOrdinal("Type")) == "Q")
                        {
                            com.CID = rdr3.GetString(rdr3.GetOrdinal("CommmentId"));
                            com.UID = rdr3.GetString(rdr3.GetOrdinal("UserId"));
                            com.Description = getXmlData(getXMLpath(com.CID, "comment"));
                        }

                        else
                        {
                            com.CID = rdr3.GetString(rdr3.GetOrdinal("CommmentId"));
                            com.UID = rdr3.GetString(rdr3.GetOrdinal("UserId"));
                            com.Description = getXmlData(getXMLpath(com.CID, "comment"));
                        }                       
                    }
                }
            }

            return com;
        }

        public File fillFileDeatailsById(string id)
        {
            File fi = new File();
            SqlCommand cmd4;
            string query = "SELECT * FROM FileTable WHERE FileId = '" + id + "'";
            cmd4 = new SqlCommand(query, this.conn);
            cmd4.ExecuteNonQuery();
            using (SqlDataReader rdr4 = cmd4.ExecuteReader())
            {
                if (rdr4 != null)
                {
                    while (rdr4.Read())
                    {
                        if (rdr4.GetString(rdr4.GetOrdinal("Type")) == "Q")
                        {
                            fi.Location = rdr4.GetString(rdr4.GetOrdinal("Location"));
                        }

                        else
                        {
                            fi.Location = rdr4.GetString(rdr4.GetOrdinal("Location"));
                        }
                    }
                }
            }

            return fi;
        }

        public string getXMLpath(string id, string type)
        {
            SqlCommand cmd5;
            string path = string.Empty;
            if(type == "comment")
            {
                string query = "SELECT * FROM A WHERE CommentId = '" + id + "'";
                cmd5 = new SqlCommand(query, this.conn);
                cmd5.ExecuteNonQuery();
                using (SqlDataReader rdr5 = cmd5.ExecuteReader())
                {
                    if (rdr5 != null)
                    {
                        while (rdr5.Read())
                        {
                            path = rdr5.GetString(rdr5.GetOrdinal("QuestionId")) + "/" + rdr5.GetString(rdr5.GetOrdinal("AnswerId")) + "/" + id;
                        }
                    }
                }
            }
            else
            {
                string query = "SELECT * FROM AnswerTable WHERE AnswerId = '" + id + "'";
                cmd5 = new SqlCommand(query, this.conn);
                cmd5.ExecuteNonQuery();
                using (SqlDataReader rdr5 = cmd5.ExecuteReader())
                {
                    if (rdr5 != null)
                    {
                        while (rdr5.Read())
                        {
                            path = rdr5.GetString(rdr5.GetOrdinal("QuestionId")) + "/" + id;
                        }
                    }
                }
            }
            
            return path;
        }

        public string getXmlData(string path)
        {
            string _xmlPath = string.Empty;
            _xmlPath = System.IO.Path.Combine("~/Content/XML Location/QandA.xml");
            XmlDocument xml = new XmlDocument();
            xml.Load(_xmlPath);            
            XmlNode xn = xml.SelectSingleNode("/Questions/" + path);    
            return xn["description"].InnerText;
        }        
    }
}