﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace letzxplore.Models
{
    public class Comment
    {
        public string CID { get; set; }

        public string UID { get; set; }

        public string Description { get; set; }

    }
}