﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace letzxplore.Models
{
    public class Answer
    {
        public string AID { get; set; }

        public string UID { get; set; }

        public string Description { get; set; }

        public int Likes { get; set; }

        public List<Comment> Comments { get; set; }

        public List<File> Files { get; set; }
    }
}