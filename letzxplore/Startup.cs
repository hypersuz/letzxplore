﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(letzxplore.Startup))]
namespace letzxplore
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
